Name:           sshj
Version:        0.13.0
Release:        11
Summary:        SSH, scp and sftp library for Java
License:        ASL 2.0
URL:            https://github.com/hierynomus/sshj
Source0:        https://github.com/hierynomus/sshj/archive/v%{version}.tar.gz
Patch0:         Fix-build-error.patch
BuildRequires:  gradle-local mvn(ch.qos.logback:logback-classic) mvn(com.jcraft:jzlib) >= 1.1.0-2 mvn(junit:junit)
BuildRequires:  mvn(net.iharder:base64) mvn(org.apache.sshd:sshd-core) mvn(org.bouncycastle:bcprov-jdk15on)
BuildRequires:  mvn(org.bouncycastle:bcpkix-jdk15on) mvn(org.mockito:mockito-core) mvn(org.slf4j:slf4j-api) perl
BuildArch:      noarch

%description
The package is a SSHv2 library for Java.

%package        help
Provides:       sshj-javadoc = %{version}-%{release}
Obsoletes:      sshj-javadoc < %{version}-%{release}
Summary:        Documentation for sshj

%description    help
Documentation for sshj.

%prep
%autosetup -p1
find . -name "*.jar" -print -delete
perl -p -e "s/mavenCentral/xmvn()\n  mavenCentral/" build.gradle > build.gradle.temp && mv build.gradle.temp build.gradle
native2ascii -encoding UTF8 src/main/java/net/schmizz/sshj/SSHClient.java src/main/java/net/schmizz/sshj/SSHClient.java
find src -name "Base64.java" -print -delete
sed -i "s|net.schmizz.sshj.common.Base64|net.iharder.Base64|" src/main/java/net/schmizz/sshj/transport/verification/OpenSSHKnownHosts.java \
  src/main/java/net/schmizz/sshj/userauth/keyprovider/OpenSSHKeyFile.java src/main/java/net/schmizz/sshj/userauth/keyprovider/PuTTYKeyFile.java
perl -p -e 's/compile "com.jcraft:jzlib:1.1.3"/compile "net.iharder:base64:2.3.8"\n  compile "com.jcraft:jzlib:1.1.3"/' build.gradle > build.gradle.temp
mv build.gradle.temp build.gradle
perl -p -e 's/task javadocJar/task javadocs(type: Javadoc) {\n  source = sourceSets.main.allJava\n}\n\ntask javadocJar/' build.gradle > build.gradle.temp
mv build.gradle.temp build.gradle
echo 'rootProject.name="sshj"' | tee -a settings.gradle
find src -name 'GssApiTest.java' -delete
%mvn_file com.hierynomus:sshj sshj
%mvn_alias com.hierynomus:sshj net.schmizz:sshj
%build
gradle -s --offline -x javadocs install
%install
%mvn_artifact build/poms/pom-default.xml build/libs/sshj-%{version}.jar
%mvn_install -J build/docs/javadoc

%files -f .mfiles
%doc CONTRIBUTORS README.adoc LICENSE NOTICE

%files help -f .mfiles-javadoc

%changelog
* Sat Nov 26 2022 yaoxin <yaoxin30@h-partners.com> - 0.13.0-11
- Fix build error

* Sat Dec 14 2019 Ling Yang <lingyang2@huawei.com> - 0.13.0-10
- Package init
